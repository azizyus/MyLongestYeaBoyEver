//
//  ViewController.swift
//  DynamiclyAddingImageToListThing-IJustForgetItsName
//
//  Created by dd on 07/03/2018.
//  Copyright © 2018 dd. All rights reserved.
//

import UIKit
import AVFoundation
import CoreData

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource  {

    
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    @IBOutlet weak var preLoader: UIActivityIndicatorView!
    
    
    //var imagePaths = ["image1.jpeg","image2.jpeg","image3.jpeg","image4.jpeg"]
    var baseUrl:String = ""
    var playerItem:AVPlayerItem?
    var player:AVPlayer?
    var pictureDownloadLink:String?
    var soundDownloadLink:String?
    
    var cellList = [YeaBoiCellData]()
    var cellListFromView = [MyCollectionViewCell]()
    var myFileFacede:FileFacede?
    let alert = UIAlertController(title: "Uyarı", message: "Kayıt Oynatılamıyor", preferredStyle: UIAlertControllerStyle.alert)
    
    
    func UIAlertActionNoRecordFoundCallback(alert: UIAlertAction!)   {
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
         self.alert.addAction(UIAlertAction(title: "Kapat", style: UIAlertActionStyle.default, handler: nil))
        
        
        self.preLoader.isHidden=true
        print("VIEW DID LOAD STARTED")
        
        
        print("TEST FETCHED DATA")
       
        for data in self.cellList
        {
            
            
        
           
            print("\(data.id) --- \(data.image) --- \(data.music)")
        }
        print("END TEST FETCHED DATA")
        
        //check db has data
        //if it has then do not load images/list again
        //otherwise load them all
        //get all json data
        //check write images to filesystem
        //fill cellView and get images from filesystem
        //play mp3 then save it
        //
    
        
       
        
        
       // ApiClient = ApiClass<YeaBoiCellData>(newBaseUrl: baseUrl)
        
       // ApiClient.get(path:"List",parameters:nil,)
        
        //link or filename
      //  playMusic(path: "tutturuu.mp3",isLocal: true)
        
        
       
    
        //addin images one by one to list
       
       //  let savedImage = saveImageDocumentDirectory(fileName: "myTest.jpeg",url: "https://ih1.redbubble.net/image.272534812.8153/sticker,375x360-//bg,ffffff.u2.png")
    
        /*
        let newCell = YeaBoiCellData()
    
        newCell.image = "parker.jpg"
        newCell.music  = "tutturuu.mp3"
        newCell.title = "Monkees"
        cellList.append(newCell)
        cellList.append(newCell)
        cellList.append(newCell)
        cellList.append(newCell)
        cellList.append(newCell)
        cellList.append(newCell)
        */
          self.myCollectionView.delegate = self
          self.myCollectionView.dataSource = self
        
          
        //absolute filename (https is ok too)
     //   saveFileDocumentDirectory(url: "http://85.109.247.41:8733/SoundRecords/GetPicture",fileName: "parker.jpg")
        
       
        
        
       
        
      
    }
    
    
    func playMusicLocal(path:String)
    {
/*
        let url = Bundle.main.url(forResource:"little_busters",withExtension:"mp3")
        
        let myAudioPlayer =  AVPlayer(url:url!)
        myAudioPlayer.play()
  */
    }
    
    
    func playMusicFromYeaBoiCell(cellData:YeaBoiCellData,indexPath:IndexPath,isLocal:Bool)
    {
        let soundFileName:String = "\(cellData.id).mp3"
        print("playing sound by cellClass --- \(soundFileName)")
        let isSoundFileExist:Bool = (self.myFileFacede?.checkFileExistInDocumentDir(fileName: soundFileName))!
        var isSoundFileCouldDownloaded:Bool = false
        
        if(isSoundFileExist == false)
        {
            self.preLoader.isHidden=false
            print("attempted sound file doesnt exist, tryin to download it")
        
            
            do
            {
                isSoundFileCouldDownloaded = (try myFileFacede?.saveFileSyncDocmentDirectory(fileName:"\(cellData.id).mp3",url:"\(baseUrl+"/"+"GetSound?id=")\(cellData.id)"))!
                
            }
            catch
            {
                print("FILE DOWNLOAD FUCKED UP")
            }
            
              print("download finisled")
            self.preLoader.isHidden=true
            if(isSoundFileCouldDownloaded)
            {
            cellData.isExist = true
            self.cellListFromView[indexPath.row].myLabel.backgroundColor = UIColor.green
            playMusic(path: "\(cellData.id).mp3", isLocal: isLocal)
            }
            else
            {
                
               
                  self.present(alert, animated: true, completion: nil)
            }
        
 }
        else
        {
            print("requested play sound by cellClass already exist so it plays from local")
              playMusic(path: "\(cellData.id).mp3", isLocal: isLocal)
            
        }
        
        
        
     
        
        
 
        
    }
    
   
    
    func playMusic(path:String,isLocal:Bool)
    {
          print("playing sound by playMusic()")
        let url:URL
        
       
       if(isLocal)
       {
        print("PLAYING  FROM LOCAL")
        url = (myFileFacede?.getProjectRootDir(fileName: path))!
        }
        else
       {
        print("PLAYING FROM SERVER")
          url = URL(string: path)!
        }
        //let url = URL(string: path)
        //playerItem = AVPlayerItem(url: url!)
       
            player=AVPlayer(url: url)
            player!.play()
            
       
        
        
        
        
    }
    
    func stopMusic()
    {
        
        player!.pause()
        
    }
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //gives count of cellList To fill CellView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        print("INITED CELLLIST COUNT =  \(self.cellList.count) ")
        return self.cellList.count
        
    }

    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath:IndexPath) {
        
        
       // playMusic(path: self.cellList[indexPath.row].music, isLocal: true)
        
        do
        {
        try playMusicFromYeaBoiCell(cellData: self.cellList[indexPath.row],indexPath: indexPath, isLocal: true)
        
        }
        catch
        {
        
        }
        
        //print("INDEX PATH:-----")
        //print(indexPath.row)
        
    }
    
    
    
    //loads array to collectionView by index
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection_cell", for: indexPath)
        as! MyCollectionViewCell
        let catPictureURL = URL(string: self.cellList[indexPath.row].image)!
        //let imageData = try? Data(contentsOf: catPictureURL)
       
        let imageData = try? Data(contentsOf:(myFileFacede?.getProjectRootDir(fileName: self.cellList[indexPath.row].image))!)
        
        
        
        if(self.cellList[indexPath.row].isExist == true)
        {
            cell.backgroundColor = UIColor.green
        }
        else
        {
             cell.backgroundColor = UIColor.red
        }
        
        cell.myImageView.image = UIImage(data:imageData!)
            //self.cellList[indexPath.row].myImageView.image
        cell.myLabel.text = self.cellList[indexPath.row].title
        
        
        
        
        self.cellListFromView.append(cell)
        
    
        return cell
        
    }
    
    
    
   
    
    


}

