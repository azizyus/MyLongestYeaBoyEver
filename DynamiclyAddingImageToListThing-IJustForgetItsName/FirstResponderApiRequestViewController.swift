//
//  FirstResponderApiRequestViewController.swift
//  DynamiclyAddingImageToListThing-IJustForgetItsName
//
//  Created by dd on 06/05/2018.
//  Copyright © 2018 dd. All rights reserved.
//

import UIKit
import CoreData
class FirstResponderApiRequestViewController: UIViewController,UIAlertViewDelegate {
 
    
    var baseUrl:String = "http://127.0.0.1:8733/SoundRecords"
    var cellList = [YeaBoiCellData]()
    var myFileFacede:FileFacede?
    var pictureDownloadLink:String?
    var soundDownloadLink:String?
    var DBResult:[YeaBois]!
    var context:NSManagedObjectContext!
    let alert = UIAlertController(title: "Uyarı", message: "Hiç kayıt bulunamadı", preferredStyle: UIAlertControllerStyle.alert)
    
    
    func UIAlertActionNoRecordFoundCallback(alert: UIAlertAction!)   {
        
        exit(0)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        alert.addAction(UIAlertAction(title: "Çıkış", style: UIAlertActionStyle.default, handler: UIAlertActionNoRecordFoundCallback))
        
        
        
        let container = DBContext()
        self.context = container.getContext()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "YeaBois")
      
        //  fetchRequest.resultType = .dictionaryResultType
     //   fetchRequest.propertiesToFetch = ["id"] // Single attribute I wanted to fetch
        //fetchRequest.returnsDistinctResults = true
        
        do {
            
            var result:[YeaBois]? = nil
            
            result = try self.context.fetch(YeaBois.fetchRequest())
          
           
            self.DBResult = result
            
            
        }  catch{
            
            print(error.localizedDescription)
            
        }
        

        self.pictureDownloadLink = baseUrl+"/"+"GetPicture?id="
        self.soundDownloadLink = baseUrl+"/"+"GetSound?id="
        self.myFileFacede = FileFacede()
        // Do any additional setup after loading the view.
        
        let ApiClient = ApiClass<YeaBoiCellData>(newBaseUrl:"http://127.0.0.1:8733/SoundRecords")
        print("INIT APICLASS")
        ApiClient.get(path: "List", parameters: [], callback:List,errorCallback: errorCallback)
    }
    
    func errorCallback()
    {
        var pictureFileName:String?
        var soundFileName:String?
         print("ERROR CALLBACK INITED")
        for row in self.DBResult
        {
            var dictionary =  Dictionary<String,Any>()
        
            pictureFileName = "\(row.id).jpg"
            soundFileName = "\(row.id).mp3"
            
            print("ID \(row.id)  TITLE \(row.title)")
            
            dictionary["id"]=row.id
            dictionary["Title"]=row.title
            
            var myYeaBoiCellData = YeaBoiCellData(from: dictionary)
            
            
            myYeaBoiCellData?.image = pictureFileName!
            
            //check is file exist and 
            if(self.myFileFacede?.checkFileExistInDocumentDir(fileName: soundFileName!) == true)
            {
                myYeaBoiCellData?.isExist = true
            }
            else
            {
                myYeaBoiCellData?.isExist = false
            }
            
            self.cellList.append(myYeaBoiCellData!)
            
            
        }
        
        if(self.DBResult.count == 0 )
        {
            print("NO SOUND FOUND COUNT IS 0")
            self.present(alert, animated: true, completion: nil)
            
        }
        
        performSegue(withIdentifier: "musicListingPage", sender: self.cellList)
       
        
    }
    
    func searchIdInCoreDataResult(id:Int16) -> Bool {
        
        
        for row in self.DBResult{
    
            if row.id == id
            {
                return true
            }
        
        }
        
        return false
    }
        
        
    
    

    func List(_ fetchedData:[YeaBoiCellData])
    {
        
        
       
      
        print("STARTED GET")
       
        var pictureFileName:String?
        var soundFileName:String?
       
        for data in fetchedData
        {
            
            
           
            if(searchIdInCoreDataResult(id:data.id))
            {
                print("\(data.title) EXISTS")
            }
            else
            {
                //coreData save
                
                
                 let myYeaBoiDescription = NSEntityDescription.entity(forEntityName: "YeaBois", in: self.context!)
                 let myYeaBoiData = NSManagedObject(entity:myYeaBoiDescription!,insertInto:self.context)
                 myYeaBoiData.setValue(data.title,forKey: "title")
                 myYeaBoiData.setValue(data.id, forKey: "id")
 
                //END coreData save
            }
           
           
            pictureFileName = "\(data.id).jpg"
            soundFileName = "\(data.id).mp3"
            
            myFileFacede?.saveFileSyncDocmentDirectory(fileName: pictureFileName!,url: "\(baseUrl+"/"+"GetPicture?id=")\(data.id)")
            //  myFileFacede?.saveFileDocumentDirectory(url: "\(baseUrl+"/"+"GetSound?id=")\(data.id)" , fileName: soundFileName! )
            
            print("PICTURE NAME \(pictureFileName)")
            //  data.music = soundFileName!
          
            //check is file exist and
            if(self.myFileFacede?.checkFileExistInDocumentDir(fileName: soundFileName!) == true)
            {
                data.isExist = true
            }
            else
            {
                data.isExist = false
            }
            
           
            data.image = pictureFileName!
            
           // data.image = pictureFileName!
           
            
            
            
           
            cellList.append(data)
            
            
            
            print(data.title)
            
        }
        do
        {
         try context.save()
         print("CONTEXT SAVED")
        }
        catch
        {
            
            print("CONTEXT ERROR")
        }
        
        print("FETCH END, initializing music listing page")
        
       performSegue(withIdentifier: "musicListingPage", sender: self.cellList)
        
    }
    
    //preparin for next page init
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        print("SEARCHING SEGUE \(segue.identifier)")
        if segue.identifier == "musicListingPage"
        {
            print("FOUND SEGUE \(segue.identifier)")
            let ViewController = segue.destination as! ViewController
            ViewController.cellList = (sender as? [YeaBoiCellData])!
            ViewController.baseUrl = self.baseUrl
            ViewController.myFileFacede = self.myFileFacede
            ViewController.soundDownloadLink = self.soundDownloadLink
            ViewController.pictureDownloadLink = self.pictureDownloadLink
            
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
