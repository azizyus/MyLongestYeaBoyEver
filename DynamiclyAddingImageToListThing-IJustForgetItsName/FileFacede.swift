//
//  FileFacede.swift
//  DynamiclyAddingImageToListThing-IJustForgetItsName
//
//  Created by dd on 06/05/2018.
//  Copyright © 2018 dd. All rights reserved.
//

import UIKit

class FileFacede {
    
    
    func checkFileExistInDocumentDir(fileName:String) -> Bool
    {
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        // lets create your destination file url
        let destinationUrl = documentsDirectoryURL.appendingPathComponent(fileName)
        print("SEARCHED FILE PATH \(destinationUrl.path)")
        
        // to check if it exists before downloading it
        if FileManager.default.fileExists(atPath: destinationUrl.path)
        {
            return true
        }
        
        
        return false
        
    }

     func saveFileDocumentDirectory(fileName:String,url:String)
    {
        
        
        if let audioUrl = URL(string: url) {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(fileName)
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                print("The file already exists at path")
                
                // if the file doesn't exist
            } else {
                
                // you can use NSURLSession.sharedSession to download the data asynchronously
                URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                    guard let location = location, error == nil else { return }
                    do {
                        // after downloading your file you need to move it to your destination url
                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        
                        print("File moved to documents folder")
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }).resume()
            }
        }
        
        
        
    }
    
    func saveFileSyncDocmentDirectory(fileName:String,url:String) -> Bool
    {
        
        if(checkFileExistInDocumentDir(fileName: fileName)==true)
        {
           print("SYNC DOWNLOAD FILE ALREADY EXIST")
        }
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(fileName)
        let fileUrl = URL(string: url)!
        do
        {
            let fileData = try Data(contentsOf:fileUrl)
              fileManager.createFile(atPath: paths, contents: fileData, attributes: nil)
        }
        catch
        {
         print("SYNC FILE DOWNLOAD FUCKED UP")
            
            if(self.checkFileExistInDocumentDir(fileName: fileName))
            {
                print("YOU NEED TO DELETED THIS FILE BECUASE ACTUALLY IT DOESNT DOWNLOADED")
            }
            else
            {
                print("FILE DOESNT CREATED SO NO NEED TO DELETED IT")
            }
            
            do
            {
                try fileManager.removeItem(atPath: paths)
                print("unnecessary created file removed (OK!)")
            }
            catch
            {
                print("unnecessary created file COULD NOT REMOVED (ERROR)")
            }
            
            return false
        }
        
        
      
        
        return true
        
    }
    
    func saveImageDocumentDirectory(fileName:String,url:String) -> String {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(fileName)
        
        let catPictureURL = URL(string: url)!
        let myImageData = try? Data(contentsOf: catPictureURL)
        let image = UIImage(data:myImageData!)
        
        
        let imageData = UIImageJPEGRepresentation(image!, 0.5)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        
        return paths
    }
    func getProjectRootDirAsString(fileName:String) -> String
    {
        return getProjectRootDir(fileName: fileName).absoluteString
    }
    func getProjectRootDir(fileName:String) -> URL
    {
        
        
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let absolutePath = documentsDirectoryURL.appendingPathComponent(fileName)
        return absolutePath
    }
    
}
