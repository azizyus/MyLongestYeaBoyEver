//
//  File.swift
//  apiTest
//
//  Created by dd on 30/03/2018.
//  Copyright © 2018 dd. All rights reserved.
//
import Foundation

class ApiClass <T:Model> {
    
    
    var baseUrl:String
  //  let session = URLSession.shared
    init(newBaseUrl:String) {
         baseUrl = newBaseUrl

    }
    
    
    func  getCombinedUrl(path:String) -> String
    {
        return baseUrl+"/"+path
    }
    
    
    func post(path:String,parameters:Any,callback:@escaping (_:Any)->())
    {
        
        
      
        let combinedUrl:String = getCombinedUrl(path: path)
        let thePostUrl = URL(string:combinedUrl)
        
      
        var request = URLRequest(url: thePostUrl!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let httpBody = try? JSONSerialization.data(withJSONObject: parameters,options: [])
        
        
        
        request.httpBody = httpBody
        
        
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let response = response
            {
                print("REPONSE START")
                print(response)
                print("REPONSE END")
            }
            
            if let data = data
            {
                do {
                    
                
                   // let json = try JSONSerialization.jsonObject(with: data, options: [])
                    
                  //  callback(json)
                    
                    
                }
                catch
                {
                    
                    print("ERROR START")
                    print(error)
                    print("ERROR END")
                }
            }
            
        }.resume()
        
        
 
        
    }
    
    
    func getSingle(path:String,parameters:Any,callback:@escaping (_:T)->())
    {
        
        
        let combinedUrl:String = getCombinedUrl(path: path)
        let theUrl = URL(string:combinedUrl)
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: theUrl!,completionHandler:
        {
            (data,response,error) in
            //its http HEADER REPONSE NOT DATA ITSELF
            if let response = response {
                //so dont need to print it except during debuging
                //  print(response)}
                
                //actually data, i mean this is exacly what u returned from controller
                if let data = data {
                    print(data)
                    do {
                        
                        
                        let jsonSingleItem  = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                        
                        
            
                        callback(T(from:jsonSingleItem as! Dictionary<String, Any>)!)
                        
                    }
                    catch
                    {
                        print(error)
                    }
                    
                    
                    
                }
                
            }
        })
        
        task.resume()
        
        
        
        
        
    }
    
    
    func get(path:String,parameters:Any,callback:@escaping (_:[T])->(),errorCallback:@escaping()->())
    {
    
        print("GET STARTED")
        let combinedUrl:String = getCombinedUrl(path: path)
        let theUrl = URL(string:combinedUrl)
       
        
        let session = URLSession.shared
        
        do //request
        {
            
            let task =  session.dataTask(with: theUrl!,completionHandler:
            {
                (data,response,error) in
                print("REQUEST SENT")
                
                if error != nil{
                    
                    errorCallback()
                    
                }
                
                //its http HEADER REPONSE NOT DATA ITSELF
                if response != nil {
                    //so dont need to print it except during debuging
                    
                    print("WE GOT RESPONSE")
                    //actually data, i mean this is exacly what u returned from controller
                    if let data = data {
                        print(data)
                        do {
                            
                            let items  = try JSONSerialization.jsonObject(with: data, options: []) as! NSArray
                            
                            
                            var myMovies = [T]()
                            
                            for jsonObject in items
                            {
                                //print(fetchedMovie)
                                myMovies.append(T.init(from:jsonObject as! Dictionary<String,Any>)!)
                                
                            }
                            
                            
                            
                            callback(myMovies)
                            
                        }
                        catch
                        {
                            print(error)
                            
                        }
                        
                        
                        
                    }
                    
                }
            })
            
            task.resume()
        }
        catch //request
        {
            print("REQUEST ERROR")
            
        }
        
        
        
     
    }
    
}
